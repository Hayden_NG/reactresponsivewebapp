import React from "react";
import {
  InfoRow,
  InfoWrapper,
  InfonContainer,
  Column1,
  Column2,
  BtnWrap,
  TextWrapper,
  Subtitle,
  Heading,
  TopLine,
  ImgWrap,
  Img,
} from "./InfoElements";
import { Button } from "../ButtonElements";

const InfoSection = ({
  lightBg,
  id,
  imgStart,
  topLine,
  lightText,
  headline,
  dark,
  darkText,
  description,
  buttonLabel,
  img,
  alt,
  primary,
}) => {
  return (
    <>
      <InfonContainer lightBg={lightBg} id={id}>
        <InfoWrapper>
          <InfoRow imgStart={imgStart}>
            <Column1>
              <TextWrapper>
                <TopLine>{topLine}</TopLine>
                <Heading lightText={lightText}>{headline}</Heading>
                <Subtitle darkText={darkText}>{description}</Subtitle>
                <BtnWrap>
                  <Button
                    to="home"
                    primary={primary ? 1 : 0}
                    dark={dark ? 1 : 0}
                    smooth={true}
                    duration={500}
                    spy={true}
                    exact="true"
                    offset={-80}
                  >
                    {buttonLabel}
                  </Button>
                </BtnWrap>
              </TextWrapper>
            </Column1>
            <Column2>
              <ImgWrap>
                <Img src={img} alt={alt} />
              </ImgWrap>
            </Column2>
          </InfoRow>
        </InfoWrapper>
      </InfonContainer>
    </>
  );
};

export default InfoSection;
